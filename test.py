from ColoredHandler import ColoredStreamHandler
import logging
import sys

logging.basicConfig(level=logging.DEBUG, filename='log.log')
sh = ColoredStreamHandler(sys.stdout)
logging.getLogger().addHandler(sh)
logging.debug('debug')
logging.info('info')
logging.warning('warning')
logging.error('error')
try:
    raise Exception('exception')
except Exception as e:
    logging.exception(e)
    
