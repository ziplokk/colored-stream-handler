import logging
from ctypes import c_ulong, windll

class ColoredStreamHandler(logging.StreamHandler):
    """
    A logging StreamHandler class that outputs colored text based
    on the set log level
    """
    DARK_BLUE = 1
    DARK_GREEN = 2
    DARK_CYAN = 3
    DARK_RED = 4
    DARK_PURPLE = 5
    DARK_YELLOW = 6
    LIGHT_GREY = 7
    DARK_GREY = 8
    BLUE = 9
    GREEN = 10
    CYAN = 11
    RED = 12
    PURPLE = 13
    YELLOW = 14
    WHITE = 15

    def __init__(self, stream=None):
        logging.StreamHandler.__init__(self, stream)
        self.std_out_handle_id = c_ulong(0xfffffff5)
        windll.Kernel32.GetStdHandle.resttype = c_ulong
        self.std_output_handle = windll.Kernel32.GetStdHandle(self.std_out_handle_id)
        self.default_formatter = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s - %(message)s', '%Y-%m-%dT%H:%M:%S')
        self.stream = stream
        
    def emit(self, record):
        level = record.levelno
        emission_color = ColoredStreamHandler.WHITE

        # Undefined
        if level == 0:
            emission_color = ColoredStreamHandler.LIGHT_GREY
        # Debug
        elif level == 10:
            emission_color = ColoredStreamHandler.GREEN
        # Info
        elif level == 20:
            emission_color = ColoredStreamHandler.LIGHT_GREY
        # Warning
        elif level == 30:
            emission_color = ColoredStreamHandler.YELLOW
        # Exception
        # Exception is logged with error then traceback so
        # this checks if level is error with a traceback and
        # sets the color to red
        elif level == 40 and record.exc_info or level == 50:
            emission_color = ColoredStreamHandler.RED
        # Error
        elif level == 40:
            emission_color = ColoredStreamHandler.PURPLE
        else:
            emission_color = ColoredStreamHandler.WHITE

        windll.Kernel32.SetConsoleTextAttribute(self.std_output_handle, emission_color)
        if bool(self.formatter):
            message = self.formatter.format(record)
        else:
            message = self.default_formatter.format(record)
        if self.stream:
            self.stream.write('%s\n' % message)
        else:
            print message
        windll.Kernel32.SetConsoleTextAttribute(self.std_output_handle, ColoredStreamHandler.WHITE)

if __name__ == '__main__':
    import sys
    
    logging.basicConfig(level=logging.DEBUG, filename='log.log')
    sh = ColoredStreamHandler(sys.stdout)
    fmtr = logging.Formatter('%(levelname)s: %(message)s')
    sh.setFormatter(fmtr)
    logging.getLogger().addHandler(sh)
    logging.debug('debug')
    logging.info('info')
    logging.warning('warning')
    logging.error('error')
    try:
        raise Exception('exception')
    except Exception as e:
        logging.exception(e)
